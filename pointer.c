#include <stdio.h>

int input()
{
    int x;
    printf("enter a number");
    scanf("%d",&x);
    return x;
}
void swap(int *x,int *y)
{
   int a=*x;
*x=*y;
*y=a; 
}
void display(int *x,int *y)
{
    printf("%d %dafter swap\n",*x,*y);
}
int main()
{
    int x,y;
    x=input();
    y=input();
    printf("%d %dbefore swap\n",x,y);
    swap(&x,&y);
    display(&x,&y);
}